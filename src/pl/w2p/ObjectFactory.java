
package pl.w2p;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pl.w2p package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetScoreResponse_QNAME = new QName("http://w2p.pl/", "getScoreResponse");
    private final static QName _ResetScore_QNAME = new QName("http://w2p.pl/", "resetScore");
    private final static QName _GetLossesResponse_QNAME = new QName("http://w2p.pl/", "getLossesResponse");
    private final static QName _IncreaseLosses_QNAME = new QName("http://w2p.pl/", "increaseLosses");
    private final static QName _IncreaseWinsResponse_QNAME = new QName("http://w2p.pl/", "increaseWinsResponse");
    private final static QName _UpdateScore_QNAME = new QName("http://w2p.pl/", "updateScore");
    private final static QName _ResetScoreResponse_QNAME = new QName("http://w2p.pl/", "resetScoreResponse");
    private final static QName _GetWins_QNAME = new QName("http://w2p.pl/", "getWins");
    private final static QName _IncreaseTies_QNAME = new QName("http://w2p.pl/", "increaseTies");
    private final static QName _IncreaseTiesResponse_QNAME = new QName("http://w2p.pl/", "increaseTiesResponse");
    private final static QName _IncreaseLossesResponse_QNAME = new QName("http://w2p.pl/", "increaseLossesResponse");
    private final static QName _UpdateScoreResponse_QNAME = new QName("http://w2p.pl/", "updateScoreResponse");
    private final static QName _GetLosses_QNAME = new QName("http://w2p.pl/", "getLosses");
    private final static QName _IncreaseWins_QNAME = new QName("http://w2p.pl/", "increaseWins");
    private final static QName _GetTiesResponse_QNAME = new QName("http://w2p.pl/", "getTiesResponse");
    private final static QName _GetScore_QNAME = new QName("http://w2p.pl/", "getScore");
    private final static QName _GetTies_QNAME = new QName("http://w2p.pl/", "getTies");
    private final static QName _GetWinsResponse_QNAME = new QName("http://w2p.pl/", "getWinsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pl.w2p
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetScoreResponse }
     * 
     */
    public GetScoreResponse createGetScoreResponse() {
        return new GetScoreResponse();
    }

    /**
     * Create an instance of {@link ResetScore }
     * 
     */
    public ResetScore createResetScore() {
        return new ResetScore();
    }

    /**
     * Create an instance of {@link GetLossesResponse }
     * 
     */
    public GetLossesResponse createGetLossesResponse() {
        return new GetLossesResponse();
    }

    /**
     * Create an instance of {@link IncreaseLosses }
     * 
     */
    public IncreaseLosses createIncreaseLosses() {
        return new IncreaseLosses();
    }

    /**
     * Create an instance of {@link UpdateScore }
     * 
     */
    public UpdateScore createUpdateScore() {
        return new UpdateScore();
    }

    /**
     * Create an instance of {@link IncreaseWinsResponse }
     * 
     */
    public IncreaseWinsResponse createIncreaseWinsResponse() {
        return new IncreaseWinsResponse();
    }

    /**
     * Create an instance of {@link GetWins }
     * 
     */
    public GetWins createGetWins() {
        return new GetWins();
    }

    /**
     * Create an instance of {@link ResetScoreResponse }
     * 
     */
    public ResetScoreResponse createResetScoreResponse() {
        return new ResetScoreResponse();
    }

    /**
     * Create an instance of {@link IncreaseTies }
     * 
     */
    public IncreaseTies createIncreaseTies() {
        return new IncreaseTies();
    }

    /**
     * Create an instance of {@link IncreaseTiesResponse }
     * 
     */
    public IncreaseTiesResponse createIncreaseTiesResponse() {
        return new IncreaseTiesResponse();
    }

    /**
     * Create an instance of {@link IncreaseLossesResponse }
     * 
     */
    public IncreaseLossesResponse createIncreaseLossesResponse() {
        return new IncreaseLossesResponse();
    }

    /**
     * Create an instance of {@link UpdateScoreResponse }
     * 
     */
    public UpdateScoreResponse createUpdateScoreResponse() {
        return new UpdateScoreResponse();
    }

    /**
     * Create an instance of {@link GetLosses }
     * 
     */
    public GetLosses createGetLosses() {
        return new GetLosses();
    }

    /**
     * Create an instance of {@link IncreaseWins }
     * 
     */
    public IncreaseWins createIncreaseWins() {
        return new IncreaseWins();
    }

    /**
     * Create an instance of {@link GetScore }
     * 
     */
    public GetScore createGetScore() {
        return new GetScore();
    }

    /**
     * Create an instance of {@link GetTies }
     * 
     */
    public GetTies createGetTies() {
        return new GetTies();
    }

    /**
     * Create an instance of {@link GetTiesResponse }
     * 
     */
    public GetTiesResponse createGetTiesResponse() {
        return new GetTiesResponse();
    }

    /**
     * Create an instance of {@link GetWinsResponse }
     * 
     */
    public GetWinsResponse createGetWinsResponse() {
        return new GetWinsResponse();
    }

    /**
     * Create an instance of {@link Score }
     * 
     */
    public Score createScore() {
        return new Score();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetScoreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getScoreResponse")
    public JAXBElement<GetScoreResponse> createGetScoreResponse(GetScoreResponse value) {
        return new JAXBElement<GetScoreResponse>(_GetScoreResponse_QNAME, GetScoreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetScore }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "resetScore")
    public JAXBElement<ResetScore> createResetScore(ResetScore value) {
        return new JAXBElement<ResetScore>(_ResetScore_QNAME, ResetScore.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLossesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getLossesResponse")
    public JAXBElement<GetLossesResponse> createGetLossesResponse(GetLossesResponse value) {
        return new JAXBElement<GetLossesResponse>(_GetLossesResponse_QNAME, GetLossesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseLosses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "increaseLosses")
    public JAXBElement<IncreaseLosses> createIncreaseLosses(IncreaseLosses value) {
        return new JAXBElement<IncreaseLosses>(_IncreaseLosses_QNAME, IncreaseLosses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseWinsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "increaseWinsResponse")
    public JAXBElement<IncreaseWinsResponse> createIncreaseWinsResponse(IncreaseWinsResponse value) {
        return new JAXBElement<IncreaseWinsResponse>(_IncreaseWinsResponse_QNAME, IncreaseWinsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateScore }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "updateScore")
    public JAXBElement<UpdateScore> createUpdateScore(UpdateScore value) {
        return new JAXBElement<UpdateScore>(_UpdateScore_QNAME, UpdateScore.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetScoreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "resetScoreResponse")
    public JAXBElement<ResetScoreResponse> createResetScoreResponse(ResetScoreResponse value) {
        return new JAXBElement<ResetScoreResponse>(_ResetScoreResponse_QNAME, ResetScoreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWins }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getWins")
    public JAXBElement<GetWins> createGetWins(GetWins value) {
        return new JAXBElement<GetWins>(_GetWins_QNAME, GetWins.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseTies }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "increaseTies")
    public JAXBElement<IncreaseTies> createIncreaseTies(IncreaseTies value) {
        return new JAXBElement<IncreaseTies>(_IncreaseTies_QNAME, IncreaseTies.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseTiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "increaseTiesResponse")
    public JAXBElement<IncreaseTiesResponse> createIncreaseTiesResponse(IncreaseTiesResponse value) {
        return new JAXBElement<IncreaseTiesResponse>(_IncreaseTiesResponse_QNAME, IncreaseTiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseLossesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "increaseLossesResponse")
    public JAXBElement<IncreaseLossesResponse> createIncreaseLossesResponse(IncreaseLossesResponse value) {
        return new JAXBElement<IncreaseLossesResponse>(_IncreaseLossesResponse_QNAME, IncreaseLossesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateScoreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "updateScoreResponse")
    public JAXBElement<UpdateScoreResponse> createUpdateScoreResponse(UpdateScoreResponse value) {
        return new JAXBElement<UpdateScoreResponse>(_UpdateScoreResponse_QNAME, UpdateScoreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLosses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getLosses")
    public JAXBElement<GetLosses> createGetLosses(GetLosses value) {
        return new JAXBElement<GetLosses>(_GetLosses_QNAME, GetLosses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseWins }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "increaseWins")
    public JAXBElement<IncreaseWins> createIncreaseWins(IncreaseWins value) {
        return new JAXBElement<IncreaseWins>(_IncreaseWins_QNAME, IncreaseWins.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getTiesResponse")
    public JAXBElement<GetTiesResponse> createGetTiesResponse(GetTiesResponse value) {
        return new JAXBElement<GetTiesResponse>(_GetTiesResponse_QNAME, GetTiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetScore }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getScore")
    public JAXBElement<GetScore> createGetScore(GetScore value) {
        return new JAXBElement<GetScore>(_GetScore_QNAME, GetScore.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTies }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getTies")
    public JAXBElement<GetTies> createGetTies(GetTies value) {
        return new JAXBElement<GetTies>(_GetTies_QNAME, GetTies.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWinsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://w2p.pl/", name = "getWinsResponse")
    public JAXBElement<GetWinsResponse> createGetWinsResponse(GetWinsResponse value) {
        return new JAXBElement<GetWinsResponse>(_GetWinsResponse_QNAME, GetWinsResponse.class, null, value);
    }

}
