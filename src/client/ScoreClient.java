package client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.w2p.Score;
import pl.w2p.ScoreService;
import pl.w2p.ScoreServiceService;

import java.net.MalformedURLException;
import java.net.URL;

public class ScoreClient {

    public static final String SERVICE_WSDL = "http://localhost:8080/web-service-server/ScoreService?wsdl";
    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreClient.class);

    public static void main(String[] args) throws MalformedURLException {

        ScoreServiceService service; //wsdl:service
        ScoreService scoreService; //wsdl:port


        URL url = new URL(SERVICE_WSDL);
        service = new ScoreServiceService(url);
        scoreService = service.getScoreServicePort();

        //methods available in remote service
        Score score = scoreService.getScore();
        LOGGER.info("1: " + score.getWins());

    }

}
