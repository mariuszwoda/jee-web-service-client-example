package client;


import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import pl.w2p.Score;
import pl.w2p.ScoreService;
import pl.w2p.ScoreServiceService;

import java.net.MalformedURLException;
import java.net.URL;

public class ScoreClientTest {

    public static final String SERVICE_WSDL = "http://localhost:8080/web-service-server/ScoreService?wsdl";

    private static final Logger log = LoggerFactory.getLogger(ScoreClientTest.class);

    ScoreServiceService service; //wsdl:service
    ScoreService scoreService; //wsdl:port

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        scoreService.resetScore();
    }

    @Test
    public void scoreTest() {
        service = new ScoreServiceService();
        scoreService = service.getScoreServicePort();

        //methods available in remote service
        Score score = scoreService.getScore();
        log.info("1: " + score.getWins());
        Assert.assertEquals(score.getWins(), 0);

        scoreService.increaseWins();
        scoreService.increaseWins();
        score = scoreService.getScore();
        log.info("2: " + score.getWins());
        Assert.assertEquals(score.getWins(), 2);
    }

    @Test
    public void scoreTestUrl() {
        URL url = null;
        try {
            url = new URL(SERVICE_WSDL);

        } catch (MalformedURLException e) {
            log.debug("exception: " + e);

        }
        service = new ScoreServiceService(url); //wsdl:service
        scoreService = service.getScoreServicePort(); //wsdl:port

        Score score = scoreService.getScore();
        log.debug("ScoreClient.main score: ", score.getWins());

        scoreService.increaseWins();
        score = scoreService.getScore();
        Assert.assertEquals(score.getWins(), 1);

        scoreService.resetScore();
        score = scoreService.getScore();
        Assert.assertEquals(score.getWins(), 0);

    }


}
